package domain.service;

import domain.*;
import org.junit.Before;
import org.junit.Test;
import service.UserService;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.*;

public class UserServiceTest {

	private static final String NAME_1 = "Bob";
	private static final String NAME_2 = "Alan";
	private static final String NAME_3 = "Name3";
	private static final String NAME_4 = "Arthur";
	private static final String NAME_5 = "Adam";

	private static final String P_NAME_1 = "xyz";
	private static final String P_NAME_2 = "def";
	private static final String P_NAME_3 = "abc";

	private Role role1;
	private Role role2;
	private Person person1;
	private Person person3;
	private User user1;
	private User user2;
	private User user3;
	private User user4;
	private User user5;
	private List<User> usersWithOneAddress, usersUnder18, nullUsers;

	@Before
	public void createUsers() {

		Permission permission1, permission2, permission3;
		Address address1, address2, address3;

		permission1 = new Permission();
		permission1.setName(P_NAME_1);
		permission2 = new Permission();
		permission2.setName(P_NAME_2);
		permission3 = new Permission();
		permission3.setName(P_NAME_3);

		role1 = new Role();
		role1.setPermissions(asList(permission1, permission2));
		role2 = new Role();
		role2.setPermissions(singletonList(permission3));
		Role role3 = new Role();
		role3.setPermissions(singletonList(permission2));

		address1 = new Address();
		address1.setCity("city1");
		address2 = new Address();
		address2.setCity("city2");
		address3 = new Address();
		address3.setCity("city3");

		person1 = new Person();
		person1.setAge(25);
		person1.setAddresses(asList(address1, address2));
		person1.setName(NAME_1);
		person1.setSurname("Surname1");
		person1.setRole(role1);

		Person person2 = new Person();
		person2.setAge(17);
		person2.setAddresses(singletonList(address1));
		person2.setName(NAME_2);
		person2.setSurname("Surname2");
		person2.setRole(role2);

		person3 = new Person();
		person3.setAge(66);
		person3.setAddresses(asList(address1, address2, address3));
		person3.setName(NAME_3);
		person3.setSurname("Surname3");
		person3.setRole(role1);

		Person person4 = new Person();
		person4.setAge(20);
		person4.setAddresses(singletonList(address1));
		person4.setName(NAME_4);
		person4.setSurname("Surname2");
		person4.setRole(role3);

		Person person5 = new Person();
		person5.setAge(30);
		person5.setAddresses(asList(address1, address3));
		person5.setName(NAME_5);
		person5.setSurname("Surname3");
		person5.setRole(role1);

		Person person6 = new Person();
		person6.setAge(1);

		user1 = new User();
		user1.setName("u1");
		user1.setPassword("p1");
		user1.setPersonDetails(person1);

		user2 = new User();
		user2.setName("user2");
		user2.setPassword("password2");
		user2.setPersonDetails(person2);

		user3 = new User();
		user3.setName("user123456789");
		user3.setPassword("pass");
		user3.setPersonDetails(person3);

		user4 = new User();
		user4.setName("person4");
		user4.setPassword("password2");
		user4.setPersonDetails(person4);

		user5 = new User();
		user5.setName("person5");
		user5.setPassword("pass");
		user5.setPersonDetails(person5);

		User user6 = new User();
		user6.setPersonDetails(person6);

		usersWithOneAddress = asList(user2, user4);
		usersUnder18 = asList(user2, user6);
		nullUsers = null;
	}

	@Test
	public void findUsersWhoHaveMoreThanOneAddress_Users_UsersFound() {
		assertThat(UserService.findUsersWhoHaveMoreThanOneAddress(asList(user2, user3, user5)))
				.isNotNull()
				.isNotEmpty()
				.containsOnly(user3, user5);

		assertThat(UserService.findUsersWhoHaveMoreThanOneAddress(asList(user1, user2, user4)))
				.isNotNull()
				.isNotEmpty()
				.containsOnly(user1);
	}

	@Test
	public void findUsersWhoHaveMoreThanOneAddress_Null_ExceptionThrown() {
		assertThatNullPointerException()
				.isThrownBy(() -> UserService.findUsersWhoHaveMoreThanOneAddress(nullUsers));
	}

	@Test
	public void findUsersWhoHaveMoreThanOneAddress_UsersWithOneAddress_EmptyResult() {
		assertThat(UserService.findUsersWhoHaveMoreThanOneAddress(usersWithOneAddress))
				.isNotNull()
				.isEmpty();
	}

	@Test
	public void findUsersWhoHaveMoreThanOneAddress_EmptyList_EmptyResult() {
		assertThat(UserService.findUsersWhoHaveMoreThanOneAddress(new ArrayList<>()))
				.isNotNull()
				.isEmpty();
	}

	@Test
	public void findOldestPerson_Users_PersonFound() {
		assertThat(UserService.findOldestPerson(asList(user2, user3, user5)))
				.isNotNull()
				.isEqualTo(person3);

		assertThat(UserService.findOldestPerson(asList(user1, user2, user4)))
				.isNotNull()
				.isEqualTo(person1);
	}

	@Test
	public void findOldestPerson_EmptyList_Null() {
		assertThat(UserService.findOldestPerson(new ArrayList<>()))
				.isNull();
	}

	@Test
	public void findOldestPerson_Null_ExceptionThrown() {
		assertThatNullPointerException()
				.isThrownBy(() -> UserService.findOldestPerson(nullUsers));
	}

	@Test
	public void getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18_Users_NamesSeparated() {
		assertThat(UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(asList(user5, user1, user2)))
				.isNotNull()
				.isNotBlank()
				.containsSequence(user5.getPersonDetails().getName() + " " + user5.getPersonDetails().getSurname()
						+ ",")
				.containsSequence(user1.getPersonDetails().getName() + " " + user1.getPersonDetails().getSurname())
				.doesNotContain(user2.getPersonDetails().getName())
				.doesNotContain(user2.getPersonDetails().getSurname());
	}

	@Test
	public void getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18_Null_ExceptionThrown() {
		assertThatNullPointerException()
				.isThrownBy(() -> UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(nullUsers));
	}

	@Test
	public void getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18_AllUsersUnder18_EmptyString() {
		assertThat(UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(usersUnder18))
				.isNotNull()
				.isBlank();
	}

	@Test
	public void getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18_EmptyList_EmptyString() {
		assertThat(UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(new ArrayList<>()))
				.isNotNull()
				.isBlank();
	}

	@Test
	public void findUserWithLongestUsername_Users_UserFound() {
		assertThat(UserService.findUserWithLongestUsername(asList(user2, user3, user5)))
				.isNotNull()
				.isEqualTo(user3);

		assertThat(UserService.findUserWithLongestUsername(asList(user1, user2, user4)))
				.isNotNull()
				.isEqualTo(user4);
	}

	@Test
	public void findUserWithLongestUsername_EmptyList_Null() {
		assertThat(UserService.findUserWithLongestUsername(new ArrayList<>()))
				.isNull();
	}

	@Test
	public void findUserWithLongestUsername_Null_ExceptionThrown() {
		assertThatNullPointerException()
				.isThrownBy(() -> UserService.findUserWithLongestUsername(nullUsers));
	}

	@Test
	public void getSortedPermissionsOfUsersWithNameStartingWithA_Users_SortedNames() {
		assertThat(UserService.getSortedPermissionsOfUsersWithNameStartingWithA(asList(user2, user3, user5)))
				.isNotNull()
				.isNotEmpty()
				.isSorted()
				.containsOnly(P_NAME_3, P_NAME_2, P_NAME_1);

		assertThat(UserService.getSortedPermissionsOfUsersWithNameStartingWithA(asList(user1, user2, user4)))
				.isNotNull()
				.isNotEmpty()
				.isSorted()
				.containsOnly(P_NAME_3, P_NAME_2);
	}

	@Test
	public void getSortedPermissionsOfUsersWithNameStartingWithA_EmptyList_EmptyResult() {
		assertThat(UserService.getSortedPermissionsOfUsersWithNameStartingWithA(new ArrayList<>()))
				.isNotNull()
				.isEmpty();
	}

	@Test
	public void getSortedPermissionsOfUsersWithNameStartingWithA_NoNamesStartingWithA_EmptyResult() {
		assertThat(UserService.getSortedPermissionsOfUsersWithNameStartingWithA(asList(user1, user3)))
				.isNotNull()
				.isEmpty();
	}

	@Test
	public void getSortedPermissionsOfUsersWithNameStartingWithA_Null_ExceptionThrown() {
		assertThatNullPointerException()
				.isThrownBy(() -> UserService.getSortedPermissionsOfUsersWithNameStartingWithA(nullUsers));
	}

	@Test
	public void groupUsersByRole_Users_UsersGrouped() {
		assertThat(UserService.groupUsersByRole(asList(user5, user1, user2)))
				.isNotNull()
				.isNotEmpty()
				.containsOnlyKeys(role1, role2)
				.containsOnly(entry(role1, asList(user5, user1)), entry(role2, singletonList(user2)));
	}

	@Test
	public void groupUsersByRole_Null_ExceptionThrown() {
		assertThatNullPointerException()
				.isThrownBy(() -> UserService.groupUsersByRole(nullUsers));
	}

	@Test
	public void groupUsersByRole_EmptyList_EmptyResult() {
		assertThat(UserService.groupUsersByRole(new ArrayList<>()))
				.isNotNull()
				.isEmpty();
	}

	@Test
	public void partitionUserByUnderAndOver18_Users_UsersPartitioned() {
		assertThat(UserService.partitionUserByUnderAndOver18(asList(user2, user3, user5)))
				.isNotNull()
				.isNotEmpty()
				.containsOnlyKeys(true, false)
				.contains(entry(true, asList(user3, user5)))
				.contains(entry(false, singletonList(user2)));
	}

	@Test
	public void partitionUserByUnderAndOver18_OnlyUsersUnder18_UsersPartitioned() {
		assertThat(UserService.partitionUserByUnderAndOver18(usersUnder18))
				.isNotNull()
				.isNotEmpty()
				.containsOnlyKeys(true, false)
				.contains(entry(false, usersUnder18))
				.contains(entry(true, emptyList()));
	}

	@Test
	public void partitionUserByUnderAndOver18_Null_ExceptionThrown() {
		assertThatNullPointerException()
				.isThrownBy(() -> UserService.partitionUserByUnderAndOver18(nullUsers));
	}

	@Test
	public void partitionUserByUnderAndOver18_EmptyList_EmptyResult() {
		assertThat(UserService.partitionUserByUnderAndOver18(new ArrayList<>()))
				.isNotNull()
				.containsOnlyKeys(true, false)
				.contains(entry(false, emptyList()))
				.contains(entry(true, emptyList()));
	}

	@Test
	public void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS_Null_ExceptionThrown() {
		assertThatNullPointerException()
				.isThrownBy(() -> UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(nullUsers));
	}
}
