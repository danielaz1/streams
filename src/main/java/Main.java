import domain.Permission;
import domain.Person;
import domain.Role;
import domain.User;
import service.UserService;

import java.util.List;

import static java.util.Arrays.asList;

public class Main {

	public static void main(String[] args) {
		System.out.println("Hello World!");

		Permission permission1 = new Permission();
		permission1.setName("abc");

		Permission permission2 = new Permission();
		permission2.setName("def");

		Permission permission3 = new Permission();
		permission3.setName("xyz");

		Role role1 = new Role();
		role1.setPermissions(asList(permission1, permission2));

		Role role2 = new Role();
		role2.setPermissions(asList(permission3));

		Person person1 = new Person();
		person1.setAge(25);
		person1.setRole(role1);
		person1.setSurname("Surname1");

		Person person2 = new Person();
		person2.setAge(100);
		person2.setRole(role2);
		person2.setSurname("Surname2");

		Person person3 = new Person();
		person3.setAge(66);
		person3.setRole(role1);
		person3.setSurname("abc");

		User user1 = new User();
		user1.setName("u1");
		user1.setPassword("p1");
		user1.setPersonDetails(person1);

		User user2 = new User();
		user2.setName("user2");
		user2.setPassword("password2");
		user2.setPersonDetails(person2);

		User user3 = new User();
		user3.setName("user123456789");
		user3.setPassword("pass");
		user3.setPersonDetails(person3);

		List<User> users = asList(user1, user2, user3);

		UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);

	}
}
